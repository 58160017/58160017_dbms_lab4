CREATE TABLE  Department(
	dnumber		INT,
	dname		VARCHAR(30),
	PRIMARY KEY(dnumber)
) ENGINE=InnoDB;

CREATE TABLE Employee(
	ssn		CHAR(9),
	name		VARCHAR(50),
	dno		INT,
	PRIMARY KEY(ssn),
	FOREIGN KEY(dno) REFERENCES Department(dnumber)
		ON DELETE SET NULL
		ON UPDATE CASCADE
) ENGINE=InnoDB;
